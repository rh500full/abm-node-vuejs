const mongoose = require('mongoose');
const router = require('express').Router();

mongoose.connect("mongodb://localhost/abmnode",{useNewUrlParser: true, useUnifiedTopology: true })

const UserSchema = new mongoose.Schema({
      username: String,
      password: String,
      email: String
   },
   {
      collection: 'users',
      versionKey: false
   }
);


const User = mongoose.model('User',UserSchema);

router.get('/',(req,res)=>{
   res.render('index.ejs');
})

router.post('/list',(req,res)=>{
   User.find({},(err,users)=>{
      if(err){ res.status(500).json({message:err})}
      res.status(200).json(users);
   })
})

router.post('/create',(req,res)=>{
   let userReq = req.body;
   newUser = new User({
      username: userReq.username,
      password: userReq.password,
      email: userReq.email
   });
   newUser.save((err,insertedUser)=>{
      if(err) res.status(500).json({message:err})
      res.status(200).json({message: "success"});
   })
})

router.post('/update',(req,res)=>{
   let userId = req.body.userId,
   userUpdate = {
      username: req.body.username,
      password: req.body.password,
      email:    req.body.email
   }   
   
   User.updateOne({'_id':userId},{$set:userUpdate},(err,result)=>{
      if(err) res.status(500).json({message:err})
      res.status(200).json({message:"success"});
   })   

})

router.post('/delete',(req,res)=>{
   let userId = req.body.userId;
   User.findByIdAndDelete(userId,(err,users)=>{
      if(err) res.status(500).json({message:err});
      res.status(200).json({message:"success"});
   })
})

module.exports = router;