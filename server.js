const bodyParser = require('body-parser');
const express = require('express');
const path = require('path');
const app = express();

//configuracion
app.set('port',process.env.PORT || 3000);
app.set('views',path.join(__dirname,'views'));
app.set('view engine','ejs');

//middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.use(require('./controllers/users'));

app.listen(app.get('port'),()=>{
   console.log(`Aplicacion corriendo en puerto ${app.get('port')}`);
})